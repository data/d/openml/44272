# OpenML dataset: Meta_Album_FNG_Micro

https://www.openml.org/d/44272

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Fungi Dataset (Micro)**
***
Meta-Album Fungi dataset is created by sampling the Danish Fungi 2020 dataset(https://arxiv.org/abs/2103.10107), itself a sampling of the Atlas of Danish Fungi repository. The images and labels which enter this database are sourced by a group consisting of 3 300 citizen botanists, then verified by their peers using a ranking of each person reliability, then finally verified by experts working at the Atlas. Of the 128 classes in the original Danish Fungi 2020 dataset, FNG retains the 25 most populous classes, belonging to six genera, for a total of 15 122 images total, with min 372, and max 1 221 images per class. Each image contains a colored 128x128 image of a fungus or a piece of a fungus from the corresponding class. Because the initial data were of widely varying sizes, we needed to crop a significant portion of the images, which we implemented by taking the largest possible square with center at the middle of the initial image. We then scaled each squared image to the 128x128 standard using the INTER_AREA anti-aliasing filter from Open-CV.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/FNG.png)

**Meta Album ID**: PLT.FNG  
**Meta Album URL**: [https://meta-album.github.io/datasets/FNG.html](https://meta-album.github.io/datasets/FNG.html)  
**Domain ID**: PLT  
**Domain Name**: Plants  
**Dataset ID**: FNG  
**Dataset Name**: Fungi  
**Short Description**: Fungi dataset from Denmark  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: fungi, ecology, plants  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: BSD-3-Clause License  
**License URL(original data release)**: https://github.com/picekl/DanishFungiDataset/blob/main/LICENSE
 
**License (Meta-Album data release)**: BSD-3-Clause License  
**License URL (Meta-Album data release)**: [https://github.com/picekl/DanishFungiDataset/blob/main/LICENSE](https://github.com/picekl/DanishFungiDataset/blob/main/LICENSE)  

**Source**: Danish Fungi Dataset  
**Source URL**: https://sites.google.com/view/danish-fungi-dataset  
  
**Original Author**: Lukas Picek, Milan Sulc, Jiri Matas, Jacob Heilmann-Clausen, Thomas S. Jeppesen, Thomas Laessoe, Tobias Froslev  
**Original contact**: lukaspicek@gmail.com  

**Meta Album author**: Felix Herron  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{picek2021danish,
    title={Danish Fungi 2020 - Not Just Another Image Recognition Dataset},
    author={Lukas Picek and Milan Sulc and Jiri Matas and Jacob Heilmann-Clausen and Thomas S. Jeppesen and Thomas Laessoe and Tobias Froslev},
    year={2021},
    eprint={2103.10107},
    archivePrefix={arXiv},
    primaryClass={cs.CV}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44302)  [[Extended]](https://www.openml.org/d/44335)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44272) of an [OpenML dataset](https://www.openml.org/d/44272). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44272/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44272/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44272/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

